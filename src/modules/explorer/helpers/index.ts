import { FolderType, FileType } from "modules/explorer/types";

export const getParentFolder = (
  rootFolder: FolderType,
  itemId: string
): FolderType | null => {
  for (let i = 0; i < rootFolder.data.length; i++) {
    const current = rootFolder.data[i];

    if (current.id === itemId) {
      return rootFolder;
    }

    if (current.type === "folder") {
      const res = getParentFolder(current, itemId);

      if (res !== null) {
        return res;
      }
    }
  }

  return null;
};

export const isChild = (
  folder: FolderType,
  item: FolderType | FileType
): boolean => {
  for (let i = 0; i < folder.data.length; i++) {
    const current = folder.data[i];

    if (current.id === item.id) {
      return true;
    }

    if (current.type === "folder") {
      const res = isChild(current, item);

      if (res) {
        return true;
      }
    }
  }

  return false;
};

const getItemById = (
  folder: FolderType,
  id: string
): FolderType | FileType | null => {
  if (folder.id === id) {
    return folder;
  }

  for (let i = 0; i < folder.data.length; i++) {
    const current = folder.data[i];

    if (current.id === id) {
      return current;
    }

    if (current.type === "folder") {
      const res = getItemById(current, id);

      if (res !== null) {
        return res;
      }
    }
  }

  return null;
};

export const deleteItemFromFolder = (
  folder: FolderType,
  idItemToDelete: string
): FolderType => {
  const folderCopy = { ...folder };

  const parentFolder = getParentFolder(folderCopy, idItemToDelete);

  if (parentFolder) {
    parentFolder.data = parentFolder.data.filter(
      (item) => item.id !== idItemToDelete
    );
  }

  return folderCopy;
};

export const addItemToFolder = (
  folder: FolderType,
  item: FolderType | FileType,
  destinationFolderId: string
): FolderType => {
  const folderCopy = { ...folder };

  const destinationItem = getItemById(folderCopy, destinationFolderId);

  if (destinationItem && destinationItem.type === "folder") {
    destinationItem.data.push(item);
  }

  return folderCopy;
};
