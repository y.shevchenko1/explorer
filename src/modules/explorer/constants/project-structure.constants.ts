import { FolderType } from "modules/explorer/types";

// ** Constants
import { EntityType } from "modules/explorer/constants/entity.constants";

export const PROJECT_STRUCTURE: FolderType = {
  id: "1",
  type: EntityType.FOLDER,
  title: "TestAssignment",
  data: [
    {
      id: "3",
      type: EntityType.FOLDER,
      title: "hooks",
      data: [
        {
          id: "4",
          type: EntityType.FILE,
          title: "useEffect.ts",
        },
        {
          id: "5",
          type: EntityType.FILE,
          title: "useState.ts",
        },
        {
          id: "6",
          type: EntityType.FILE,
          title: "useMemo.ts",
        },
        {
          id: "7",
          type: EntityType.FILE,
          title: "useCallback.ts",
        },
      ],
    },
    {
      id: "8",
      type: EntityType.FOLDER,
      title: "pages",
      data: [
        {
          id: "9",
          type: EntityType.FILE,
          title: "first.ts",
        },
        {
          id: "10",
          type: EntityType.FILE,
          title: "second.ts",
        },
        {
          id: "11",
          type: EntityType.FILE,
          title: "third.ts",
        },
        {
          id: "12",
          type: EntityType.FILE,
          title: "fourth.ts",
        },
      ],
    },
    {
      id: "13",
      type: EntityType.FOLDER,
      title: "lib",
      data: [
        {
          id: "14",
          type: EntityType.FILE,
          title: "first.ts",
        },
        {
          id: "15",
          type: EntityType.FILE,
          title: "second.ts",
        },
        {
          id: "16",
          type: EntityType.FILE,
          title: "third.ts",
        },
        {
          id: "17",
          type: EntityType.FILE,
          title: "fourth.ts",
        },
      ],
    },
    {
      id: "18",
      type: EntityType.FOLDER,
      title: "redux",
      data: [],
    },
  ],
};
