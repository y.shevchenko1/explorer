import { FolderType, FileType } from "modules/explorer/types";

export type EntityOptionProps = { entity: FolderType | FileType };
