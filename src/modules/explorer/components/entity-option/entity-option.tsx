import { FC } from "react";

// ** Components
import { Folder, File } from "modules/explorer/components";

// ** Types
import { EntityOptionProps } from "./entity-option.interfaces";

export const EntityOption: FC<EntityOptionProps> = ({ entity }) => {
  return (
    <>
      {entity.type === "folder" && <Folder folder={entity} />}
      {entity.type === "file" && <File file={entity} />}
    </>
  );
};
