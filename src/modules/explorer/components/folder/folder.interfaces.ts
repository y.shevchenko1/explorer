import { FolderType } from "modules/explorer/types";

export type FolderProps = { folder: FolderType; initialOpen?: boolean };
