import { FC, useState } from "react";
import {
  DragSourceMonitor,
  DropTargetMonitor,
  useDrag,
  useDrop,
} from "react-dnd";
import classnames from "classnames";

// ** Context
import { useExplorer } from "modules/explorer/contexts/explorer.context";

// ** Components
import { BaseIcon } from "modules/core/components";
import { EntityOption } from "modules/explorer/components";

// ** Constants
import { EntityType } from "modules/explorer/constants/entity.constants";

// ** Types
import { FolderProps } from "./folder.interfaces";
import { FolderType, FileType } from "modules/explorer/types";

// ** Styles
import styles from "./folder.module.scss";

export const Folder: FC<FolderProps> = ({ initialOpen = false, folder }) => {
  const [isOpenFolder, setIsOpenFolder] = useState<boolean>(initialOpen);

  const { handleCanDrop, handleDrop } = useExplorer();

  const [, drag] = useDrag<FolderType, unknown, { isDragging: boolean }>({
    type: EntityType.FOLDER,
    item: folder,
    collect: (monitor: DragSourceMonitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const [{ isOver, canDrop }, drop] = useDrop<
    FolderType | FileType,
    unknown,
    { isOver: boolean; canDrop: boolean }
  >({
    accept: [EntityType.FILE, EntityType.FOLDER],
    drop: (item, monitor) => {
      const didDrop = monitor.didDrop();
      if (didDrop) return;

      handleDrop(folder, item);
    },
    canDrop: (item) => {
      return handleCanDrop(folder, item);
    },
    collect: (monitor: DropTargetMonitor) => ({
      isOver: monitor.isOver({ shallow: true }),
      canDrop: monitor.canDrop(),
    }),
  });

  const handleClick = () => {
    setIsOpenFolder((prevState) => !prevState);
  };

  return (
    <div
      className={classnames(styles.folder, {
        [styles.folder_dropping]: isOver && canDrop,
      })}
      ref={(node) => drag(drop(node))}
    >
      <div className={styles.folderTitle} onClick={handleClick}>
        <BaseIcon type={isOpenFolder ? "arrowDown" : "arrowRight"} />
        <BaseIcon type="folder" className={styles.icon} />
        <span>{folder.title}</span>
      </div>
      {isOpenFolder && (
        <div className={styles.list}>
          {folder.data.map((entity) => (
            <EntityOption key={entity.id} entity={entity} />
          ))}
        </div>
      )}
    </div>
  );
};
