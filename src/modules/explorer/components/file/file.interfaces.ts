import { FileType } from "modules/explorer/types";

export type FileProps = { file: FileType };
