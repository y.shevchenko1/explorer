import { FC } from "react";
import { useDrag } from "react-dnd";

// ** Components
import { BaseIcon } from "modules/core/components";

// ** Types
import { FileProps } from "./file.interfaces";
import { FileType } from "modules/explorer/types";

// ** Constants
import { EntityType } from "modules/explorer/constants/entity.constants";

// ** Styles
import styles from "./file.module.scss";

export const File: FC<FileProps> = ({ file }) => {
  const [, drag] = useDrag<FileType, unknown, unknown>({
    type: EntityType.FILE,
    item: file,
  });

  return (
    <div className={styles.file} ref={drag}>
      <BaseIcon type="file" className={styles.icon} />
      <span>{file.title}</span>
    </div>
  );
};
