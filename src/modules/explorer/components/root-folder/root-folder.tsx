import { FC } from "react";

// ** Context
import { useExplorer } from "modules/explorer/contexts/explorer.context";

// ** Components
import { Folder } from "modules/explorer/components";

export const RootFolder: FC = () => {
  const { projectStructure } = useExplorer();

  return <Folder initialOpen folder={projectStructure} />;
};
