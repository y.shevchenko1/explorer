import { FC } from "react";

// ** Providers
import { ExplorerProvider } from "modules/explorer/providers/explorer-provider/explorer-provider";

// ** Constants
import { PROJECT_STRUCTURE } from "modules/explorer/constants/project-structure.constants";

// ** Components
import { RootFolder } from "modules/explorer/components";

// ** Styles
import styles from "./explorer.module.scss";

export const Explorer: FC = () => {
  return (
    <div className={styles.wrapper}>
      <ExplorerProvider initialProjectStructure={PROJECT_STRUCTURE}>
        <RootFolder />
      </ExplorerProvider>
    </div>
  );
};
