export type Entity = { id: string; title: string };

export type FileType = Entity & { type: "file" };

export type FolderType = Entity & {
  type: "folder";
  data: (FolderType | FileType)[];
};
