import { createContext, useContext } from "react";

// ** Types
import { FolderType, FileType } from "modules/explorer/types";

export type ExplorerValues = {
  projectStructure: FolderType;
  handleCanDrop: (
    currentFolder: FolderType,
    dropItem: FolderType | FileType
  ) => boolean;
  handleDrop: (
    currentFolder: FolderType,
    dropItem: FolderType | FileType
  ) => void;
};

export const ExplorerContext = createContext<ExplorerValues>({
  projectStructure: {
    id: crypto.randomUUID(),
    data: [],
    title: "initialFolder",
    type: "folder",
  },
  handleCanDrop: () => false,
  handleDrop: () => {},
});

export const useExplorer = () => {
  const explorerContext = useContext(ExplorerContext);

  if (!explorerContext) throw new Error("error");

  return explorerContext;
};
