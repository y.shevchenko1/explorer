import { PropsWithChildren } from "react";

// ** Types
import { FolderType } from "modules/explorer/types";

export type ExplorerProviderProps = PropsWithChildren & {
  initialProjectStructure: FolderType;
};
