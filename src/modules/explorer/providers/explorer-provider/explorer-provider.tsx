import { FC, useState, useMemo, useCallback } from "react";

// ** Context
import {
  ExplorerContext,
  ExplorerValues,
} from "modules/explorer/contexts/explorer.context";

// ** Helpers
import {
  getParentFolder,
  isChild,
  addItemToFolder,
  deleteItemFromFolder,
} from "modules/explorer/helpers";

// ** Types
import { FolderType, FileType } from "modules/explorer/types";
import { ExplorerProviderProps } from "./explorer-provider.interfaces";

export const ExplorerProvider: FC<ExplorerProviderProps> = ({
  children,
  initialProjectStructure,
}) => {
  const [projectStructure, setProjectStructure] = useState<FolderType>(
    initialProjectStructure
  );

  const handleCanDrop = useCallback(
    (
      currentItem: FolderType | FileType,
      targetItem: FolderType | FileType
    ): boolean => {
      const parentFolder = getParentFolder(projectStructure, targetItem.id);

      if (parentFolder && parentFolder.id === currentItem.id) {
        return false;
      }

      if (targetItem.type === "folder" && targetItem.id === currentItem.id) {
        return false;
      }

      if (
        targetItem.type === "folder" &&
        currentItem.type === "folder" &&
        isChild(targetItem, currentItem)
      ) {
        return false;
      }

      return true;
    },
    [projectStructure]
  );

  const handleDrop = useCallback(
    (currentItem: FolderType, targetItem: FolderType | FileType) => {
      const projectStructureWithoutTargetItem = deleteItemFromFolder(
        projectStructure,
        targetItem.id
      );

      const updatedProjectStructure = addItemToFolder(
        projectStructureWithoutTargetItem,
        targetItem,
        currentItem.id
      );

      setProjectStructure(updatedProjectStructure);
    },
    [projectStructure]
  );

  const contextValues: ExplorerValues = useMemo(
    () => ({
      projectStructure,
      handleCanDrop,
      handleDrop,
    }),
    [projectStructure, handleCanDrop, handleDrop]
  );

  return (
    <ExplorerContext.Provider value={contextValues}>
      {children}
    </ExplorerContext.Provider>
  );
};
