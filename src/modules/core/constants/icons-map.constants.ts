import { ChevronDown, ChevronRight, Folder, File } from "react-feather";

// ** Types
import { BaseIconMap } from "modules/core/components/base-icon/base-icon.interfaces";

export const ICONS_MAP: BaseIconMap = {
  arrowRight: ChevronRight,
  arrowDown: ChevronDown,
  folder: Folder,
  file: File,
};
