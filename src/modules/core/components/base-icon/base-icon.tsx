import { FC } from "react";
import { Icon } from "react-feather";

// ** Types
import { BaseIconProps } from "./base-icon.interfaces";

// ** Constants
import { ICONS_MAP } from "modules/core/constants/icons-map.constants";

export const BaseIcon: FC<BaseIconProps> = ({
  type,
  size = 18,
  ...restProps
}) => {
  const IconComponent: Icon = ICONS_MAP[type];

  return <IconComponent size={size} {...restProps} />;
};
