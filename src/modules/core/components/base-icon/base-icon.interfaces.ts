import { Icon, IconProps } from "react-feather";

export type BaseIconType = "arrowRight" | "arrowDown" | "folder" | "file";

export type BaseIconMap = Record<BaseIconType, Icon>;

export type BaseIconProps = { type: BaseIconType } & IconProps;
