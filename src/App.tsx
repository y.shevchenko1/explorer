// ** Components
import { Explorer } from "modules/explorer/containers";

// ** Styles
import "normalize-css";

export const App = () => {
  return <Explorer />;
};

export default App;
